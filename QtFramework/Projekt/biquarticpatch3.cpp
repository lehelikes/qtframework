#include "biquarticpatch3.h"

#include "BasisFunctions.h"

namespace cagd
{
    BiquarticPatch3::BiquarticPatch3() : TensorProductSurface3(0, 1, 0, 1, 4, 4)
    {
    }

    GLboolean BiquarticPatch3::UBlendingFunctionValues(GLdouble u_knot, RowMatrix<GLdouble>& blending_values) const
    {
        if (u_knot < 0.0 && u_knot > 1.0)
            return GL_FALSE;

        blending_values.ResizeColumns(4);
        blending_values[0] = F0(u_knot);
        blending_values[1] = F1(u_knot);
        blending_values[2] = F2(u_knot);
        blending_values[3] = F3(u_knot);

        return GL_TRUE;
    }

    GLboolean BiquarticPatch3::VBlendingFunctionValues(GLdouble v_knot, RowMatrix<GLdouble>& blending_values) const
    {
        if (v_knot < 0.0 && v_knot > 1.0)
            return GL_FALSE;

        blending_values.ResizeColumns(4);
        blending_values[0] = F0(v_knot);
        blending_values[1] = F1(v_knot);
        blending_values[2] = F2(v_knot);
        blending_values[3] = F3(v_knot);

        return GL_TRUE;
    }

    GLboolean BiquarticPatch3::CalculatePartialDerivatives(GLuint maximum_order_of_partial_derivatives,
                                          GLdouble u, GLdouble v, PartialDerivatives& pd) const
    {
        if (u < 0.0 || u > 1.0 || v < 0.0 || v > 1.0 || maximum_order_of_partial_derivatives > 1)
            return GL_FALSE;

        RowMatrix<GLdouble> u_blending_values(4), d1_u_blending_values(4);

        u_blending_values[0] = F0(u);
        u_blending_values[1] = F1(u);
        u_blending_values[2] = F2(u);
        u_blending_values[3] = F3(u);

        d1_u_blending_values[0] = d1F0(u);
        d1_u_blending_values[1] = d1F1(u);
        d1_u_blending_values[2] = d1F2(u);
        d1_u_blending_values[3] = d1F3(u);

        RowMatrix<GLdouble> v_blending_values(4), d1_v_blending_values(4);
        v_blending_values[0] = F0(v);
        v_blending_values[1] = F1(v);
        v_blending_values[2] = F2(v);
        v_blending_values[3] = F3(v);

        d1_v_blending_values[0] = d1F0(v);
        d1_v_blending_values[1] = d1F1(v);
        d1_v_blending_values[2] = d1F2(v);
        d1_v_blending_values[3] = d1F3(v);

        pd.ResizeRows(2);
        pd.LoadNullVectors();
        for (GLuint row = 0; row < 4; ++row) {
            DCoordinate3 aux_d0_v, aux_d1_v;
            for (GLuint column = 0; column < 4; ++column) {
                aux_d0_v += _data(row, column) * v_blending_values(column);
                aux_d1_v += _data(row, column) * d1_v_blending_values(column);
            }
            pd(0, 0) += aux_d0_v * u_blending_values(row);
            pd(1, 0) += aux_d0_v * d1_u_blending_values(row);
            pd(1, 1) += aux_d1_v * u_blending_values(row);
        }
        return GL_TRUE;
    }
}
