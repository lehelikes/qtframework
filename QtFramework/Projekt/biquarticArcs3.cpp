#include "biquarticArcs3.h"
#include "BasisFunctions.h"

namespace cagd
{
    BiquarticArcs3::BiquarticArcs3(GLenum data_usage_flag) : LinearCombination3(0.0, 1.0, 4, data_usage_flag)
    {
    }

    GLboolean BiquarticArcs3::BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const
    {
        if (u < 0.0 || u > 1.0)
        {
            return GL_FALSE;
        }

        values.ResizeColumns(4);
        values[0] = F0(u);
        values[1] = F1(u);
        values[2] = F2(u);
        values[3] = F3(u);

        return GL_TRUE;
    }

    GLboolean BiquarticArcs3::CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives& d) const
    {
        if (u < 0.0 || u > 1.0 || max_order_of_derivatives > 2)
        {
            return GL_FALSE;
        }

        d.ResizeRows(max_order_of_derivatives + 1);
        d.LoadNullVectors();

        Matrix<GLdouble> blending_values(max_order_of_derivatives + 1, 4);

        if (max_order_of_derivatives >= 0) {
            blending_values(0, 0) = F0(u);
            blending_values(0, 1) = F1(u);
            blending_values(0, 2) = F2(u);
            blending_values(0, 3) = F3(u);
        }
        if (max_order_of_derivatives >= 1) {
            blending_values(1, 0) = d1F0(u);
            blending_values(1, 1) = d1F1(u);
            blending_values(1, 2) = d1F2(u);
            blending_values(1, 3) = d1F3(u);
        }
        if (max_order_of_derivatives == 2) {
            blending_values(2, 0) = d2F0(u);
            blending_values(2, 1) = d2F1(u);
            blending_values(2, 2) = d2F2(u);
            blending_values(2, 3) = d2F3(u);
        }

        for (GLuint i = 0; i <= max_order_of_derivatives; ++i)
        {
            for (GLuint j = 0; j < 4; ++j)
            {
                d[i] += _data[j] * blending_values(i, j);
            }
        }

        return GL_TRUE;
    }
}
