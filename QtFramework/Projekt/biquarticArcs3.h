#ifndef BIQUARTICARCS3_H
#define BIQUARTICARCS3_H

#include <Core/Constants.h>
#include <Core/LinearCombination3.h>

namespace cagd
{
    class BiquarticArcs3 : public LinearCombination3
    {
    public:
        BiquarticArcs3(GLenum data_usage_flag = GL_STATIC_DRAW);

        GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const;
        GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives& d) const;
    };
}

#endif // BIQUARTICARCS3_H
