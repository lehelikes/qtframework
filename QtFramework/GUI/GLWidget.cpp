#include "GLWidget.h"

#if !defined(__APPLE__)
#include <GL/glu.h>
#endif

#include <iostream>
#include <fstream>
#include <QTimer>
using namespace std;

#include <Core/Exceptions.h>
#include <Core/Matrices.h>
#include <Core/RealSquareMatrices.h>
#include <Core/DCoordinates3.h>
#include <Core/Constants.h>

// Projekt
#include "Projekt/biquarticArcs3.h"

namespace cagd
{
    //--------------------------------
    // special and default constructor
    //--------------------------------
    GLWidget::GLWidget(QWidget *parent): QOpenGLWidget(parent)
    {
//        _timer = new QTimer(this);
//        _timer->setInterval(0);
//        connect(_timer, SIGNAL(timeout()), this, SLOT(_animate()));

    }

    GLWidget::~GLWidget()
    {
        if (_homework_id == 2)
        {
            _destroyAllExistingParametricCurvesAndTheirImages();
        }
    }

    //--------------------------------------------------------------------------------------
    // this virtual function is called once before the first call to paintGL() or resizeGL()
    //--------------------------------------------------------------------------------------
    void GLWidget::initializeGL()
    {
        // creating a perspective projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = static_cast<double>(width()) / static_cast<double>(height());
        _z_near = 1.0;
        _z_far  = 1000.0;
        _fovy   = 45.0;

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // setting the model view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        _eye[0] = _eye[1] = 0.0; _eye[2] = 6.0;
        _center[0] = _center[1] = _center[2] = 0.0;
        _up[0] = _up[2] = 0.0; _up[1] = 1.0;

        gluLookAt(_eye[0], _eye[1], _eye[2], _center[0], _center[1], _center[2], _up[0], _up[1], _up[2]);

        // enabling the depth test
        glEnable(GL_DEPTH_TEST);

        // setting the background color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // initial values of transformation parameters
        _angle_x = _angle_y = _angle_z = 0.0;
        _trans_x = _trans_y = _trans_z = 0.0;
        _zoom = 1.0;

        // ...

        try
        {
            // initializing the OpenGL Extension Wrangler library
            GLenum error = glewInit();

            if (error != GLEW_OK)
            {
                throw Exception("Could not initialize the OpenGL Extension Wrangler Library!");
            }

            if (!glewIsSupported("GL_VERSION_2_0"))
            {
                throw Exception("Your graphics card is not compatible with OpenGL 2.0+! "
                                "Try to update your driver or buy a new graphics adapter!");
            }

            // create and store your geometry in display lists or vertex buffer objects
            // ...

            switch (_homework_id)
            {
                case 1:
                    // perform simple test cases
                    _privateTestFunction();
                    break;

                case 2:
                    // create and store your geometry in display lists or vertex buffer objects,
                    // test for errors
                    if (!_createAllParametricCurvesAndTheirImages())
                    {
                        throw Exception("Could not create all parametric curves!");
                    }
                    break;
                case 3: {
//                    if (!_mouse.LoadFromOFF("models/mouse.off", GL_TRUE)) {
//                        throw Exception("Could not load model file!");
//                    }

//                    if (!_mouse.UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)) {
//                        throw Exception("Could not update the vertex buffer objects of the triangulated mesh!");
//                    }

//                    HCoordinate3 direction(0.0, 0.0, 1.0, 0.0);
//                    Color4 ambient(0.4, 0.4, 0.4, 1.0);
//                    Color4 diffuse(0.8, 0.8, 0.8, 1.0);
//                    Color4 specular(1.0, 1.0, 1.0, 1.0);

//                    _dl = new (nothrow) DirectionalLight(GL_LIGHT0, direction, ambient, diffuse, specular);
//                    if (!_dl) {
//                        throw Exception("Could not create the directional light object!");
//                    }

//                    glEnable(GL_LIGHTING);
//                    glEnable(GL_NORMALIZE);

//                    _angle = 0.0;
//                    _timer->start();

                    // Init textures
                    _texture_list.ResizeColumns(_texture_count);
                    _texture_list[0] = new QOpenGLTexture(QImage("textures/periodic_texture_00.jpg").mirrored());
                    _texture_list[1] = new QOpenGLTexture(QImage("textures/periodic_texture_01.jpg").mirrored());
                    _texture_list[2] = new QOpenGLTexture(QImage("textures/periodic_texture_02.jpg").mirrored());
                    _texture_list[3] = new QOpenGLTexture(QImage("textures/periodic_texture_03.jpg").mirrored());
                    _texture_list[4] = new QOpenGLTexture(QImage("textures/periodic_texture_04.jpg").mirrored());
                    _texture_list[5] = new QOpenGLTexture(QImage("textures/periodic_texture_05.jpg").mirrored());
                    _texture_list[6] = new QOpenGLTexture(QImage("textures/periodic_texture_06.jpg").mirrored());
                    _texture_list[7] = new QOpenGLTexture(QImage("textures/periodic_texture_07.jpg").mirrored());
                    _texture_list[8] = new QOpenGLTexture(QImage("textures/periodic_texture_08.jpg").mirrored());
                    _texture_list[9] = new QOpenGLTexture(QImage("textures/periodic_texture_09.jpg").mirrored());
                    for (GLuint i = 0; i < _texture_count; i++)
                    {
                        _texture_list[i]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
                        _texture_list[i]->setMagnificationFilter(QOpenGLTexture::Linear);
                    }

                    if (!_createAllParametricSurfacesAndTheirImages())
                    {
                        throw Exception("Could not create all parametric curves!");
                    }
                    break;
                }
                case 33: { //// castle
                    if (!_init_castle())
                    {
                        throw Exception("Could not init. the castle!");
                    }

                    break;
                }
                case 4:
                    if (!_init_cyclic_curve())
                    {
                        throw Exception("Could not init. the cyclic curve!");
                    }
                    break;
                case 44:
                    if(!_init_biquartic())
                    {
                        throw Exception("Could not init. the biquartic arc!");
                    }
                    break;
                case 444: {
                    HCoordinate3 direction(0.0, 0.0, 1.0, 0.0);
                    Color4 ambient(0.4, 0.4, 0.4, 1.0);
                    Color4 diffuse(0.8, 0.8, 0.8, 1.0);
                    Color4 specular(1.0, 1.0, 1.0, 1.0);

                    _dl = new (nothrow) DirectionalLight(GL_LIGHT0, direction, ambient, diffuse, specular);
                    if (!_dl) {
                        throw Exception("Could not create the directional light object!");
                    }

                    glEnable(GL_LIGHTING);
                    glEnable(GL_NORMALIZE);

                    if(!_init_patch())
                    {
                        throw Exception("Could not init. the biquartic patch!");
                    }
                    }
                    break;
                case 5:
                    _installShaders();
                    _loadSomeModels();
                break;
                default:
                       throw Exception("This homework ID does not exist!");
                }
        }
        catch (Exception &e)
        {
            cout << e << endl;
        }

        glEnable(GL_POINT_SMOOTH);
        glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);

        glEnable(GL_LINE_SMOOTH);
        glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);

        glEnable(GL_POLYGON_SMOOTH);
        glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);

        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

        glEnable(GL_DEPTH_TEST);
    }

    //-----------------------
    // the rendering function
    //-----------------------
    void GLWidget::paintGL()
    {
        // clears the color and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            // stores/duplicates the original model view matrix
            glPushMatrix();

            // applying transformations
            glRotatef(_angle_x, 1.0, 0.0, 0.0);
            glRotatef(_angle_y, 0.0, 1.0, 0.0);
            glRotatef(_angle_z, 0.0, 0.0, 1.0);
            glTranslated(_trans_x, _trans_y, _trans_z);
            glScaled(_zoom, _zoom, _zoom);

            // render your geometry (this is oldest OpenGL rendering technique, later we will use some advanced methods)

            switch (_homework_id) // EZT IDE KELL TENNI?
            {
                case 1:
                    break;
                case 2:
                    if (!_renderSelectedParametricCurve(_firstOrderDerivative, _secondOrderDerivative))
                    {
                        throw Exception("Could not render selected parametric curve.");
                    }
                    break;
                case 3:
//                    _dl->Enable();
//                    _mouse.Render();
//                    _dl->Disable();

                    if (!_renderSelectedParametricSurface())
                    {
                        throw Exception("Could not render selected parametric surface.");
                    }
                    break;
                case 33:
                    if (!_render_castle())
                    {
                        throw Exception("Could not render the castle!");
                    }
                    break;
                case 4:
                    if (!_render_cyclic_curve()) {
                        throw Exception("Could not render the cyclic curve!");
                    }
                    break;
                case 44:
                    if (!_render_biquartic())
                    {
                        throw Exception("Could not render the biquartic curve!");
                    }
                    break;
                case 444:
                    if (_dl)
                    {
                          _dl->Enable();

                          if (!_render_patch())
                          {
                              throw Exception("Could not render the biquartic patch!");
                          }

                          _dl->Disable();
                    }
                    break;
                case 5:
                    _renderSpotWithShaders();
                break;
                default:
                    throw Exception("This homework ID does not exist!");
            }

        // pops the current matrix stack, replacing the current matrix with the one below it on the stack,
        // i.e., the original model view matrix is restored
        glPopMatrix();
    }

    //----------------------------------------------------------------------------
    // when the main window is resized one needs to redefine the projection matrix
    //----------------------------------------------------------------------------
    void GLWidget::resizeGL(int w, int h)
    {
        // setting the new size of the rendering context
        glViewport(0, 0, w, h);

        // redefining the projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = static_cast<double>(w) / static_cast<double>(h);

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // switching back to the model view matrix
        glMatrixMode(GL_MODELVIEW);

        update();
    }

    //-----------------------------------
    // implementation of the public slots
    //-----------------------------------

    void GLWidget::set_angle_x(int value)
    {
        if (_angle_x != value)
        {
            _angle_x = value;
            update();
        }
    }

    void GLWidget::set_angle_y(int value)
    {
        if (_angle_y != value)
        {
            _angle_y = value;
            update();
        }
    }

    void GLWidget::set_angle_z(int value)
    {
        if (_angle_z != value)
        {
            _angle_z = value;
            update();
        }
    }

    void GLWidget::set_zoom_factor(double value)
    {
        if (_zoom != value)
        {
            _zoom = value;
            update();
        }
    }

    void GLWidget::set_trans_x(double value)
    {
        if (_trans_x != value)
        {
            _trans_x = value;
            update();
        }
    }

    void GLWidget::set_trans_y(double value)
    {
        if (_trans_y != value)
        {
            _trans_y = value;
            update();
        }
    }

    void GLWidget::set_trans_z(double value)
    {
        if (_trans_z != value)
        {
            _trans_z = value;
            update();
        }
    }

    void GLWidget::setSelectedCurve(int value)
    {
        // Be careful because it is not checked wether value is a corect value (the curve must exist!)
        if (value != _selected_pc) {
            _selected_pc = value;
            update();
        }
    }

    void GLWidget::setSelectedSurface(int value)
    {
        // Be careful because it is not checked wether value is a corect value (the curve must exist!)
        if (value != _selected_ps) {
            _selected_ps = value;
            update();
        }
    }

    void GLWidget::setSelectedTexture(int value)
    {
        if (GLuint(value) != _selected_texture) {
            _selected_texture = value;
            update();
        }
    }

    void GLWidget::setPointCount(int value)
    {
        if ((GLuint) value != _div_point_count) {
            _div_point_count = value;

            delete _image_of_pc[_selected_pc];
            _image_of_pc[_selected_pc] = _pc[_selected_pc]->GenerateImage(_div_point_count, _usage_flag);

            if (!_image_of_pc[_selected_pc] || !_image_of_pc[_selected_pc]->UpdateVertexBufferObjects(_scaleFirstOrderDerivative, _scaleSecondOrderDerivative, _usage_flag))
            {
                _destroyAllExistingParametricCurvesAndTheirImages();
                throw Exception("Could not update the point count.");
            }
        }

        update();
    }

    void GLWidget::setPointCount_u(int value)
    {
        if ((GLuint) value != _div_point_count_u) {
            _div_point_count_u = value;

            delete _image_of_ps[_selected_ps];
            _image_of_ps[_selected_ps] = _ps[_selected_ps]->GenerateImage(_div_point_count_u, _div_point_count_v, _usage_flag);

            if (!_image_of_ps[_selected_ps] || !_image_of_ps[_selected_ps]->UpdateVertexBufferObjects(_usage_flag))
            {
                _destroyAllExistingParametricSurfacesAndTheirImages();
                throw Exception("Could not update the point count of u.");
            }
        }

        update();
    }

    void GLWidget::setPointCount_v(int value)
    {
        if ((GLuint) value != _div_point_count_v) {
            _div_point_count_v = value;

            delete _image_of_ps[_selected_ps];
            _image_of_ps[_selected_ps] = _ps[_selected_ps]->GenerateImage(_div_point_count_u, _div_point_count_v, _usage_flag);

            if (!_image_of_ps[_selected_ps] || !_image_of_ps[_selected_ps]->UpdateVertexBufferObjects(_usage_flag))
            {
                _destroyAllExistingParametricSurfacesAndTheirImages();
                throw Exception("Could not update the point count of u.");
            }
        }

        update();
    }

    bool GLWidget::customScaleParametricCurves()
    {
        for (GLuint i = 0; i < _pc_count; i++)
        {
            if (!_image_of_pc[i] || !_image_of_pc[i]->UpdateVertexBufferObjects(_scaleFirstOrderDerivative, _scaleSecondOrderDerivative, _usage_flag))
            {
                _destroyAllExistingParametricCurvesAndTheirImages();
                return false;
            }
        }

        return true;
    }

    void GLWidget::setScaleFirstOrderDerivative(double value)
    {
        if (value != _scaleFirstOrderDerivative) {
            _scaleFirstOrderDerivative = value;

            if (!customScaleParametricCurves()) {
                throw Exception("Could not scale with custom values the parametric curves.");
            }

            update();
        }
    }

    void GLWidget::setScaleSecondOrderDerivative(double value)
    {
        if (value != _scaleSecondOrderDerivative) {
            _scaleSecondOrderDerivative = value;

            if (!customScaleParametricCurves()) {
                throw Exception("Could not scale with custom values the parametric curves.");
            }

            update();
        }
    }

    void GLWidget::showTextures(int value)
    {
        if ((GLuint) value != _display_textures)
        {
            _display_textures = value;
            update();
        }
    }

    void GLWidget::showNormalVectors(int value)
    {
        if ((GLuint) value != _display_normal_vectors)
        {
            _display_normal_vectors = value;
            update();
        }
    }

    void GLWidget::setScaleNormalVectors(double value)
    {
        if (value != _scale_normals) {
            _scale_normals = value;
            update();
        }
    }

    void GLWidget::showFirstOrderDerivative(int value)
    {
        if (value != _firstOrderDerivative) {
            _firstOrderDerivative = value;
            update();
        }
    }

    void GLWidget::showSecondOrderDerivative(int value)
    {
        if (value != _secondOrderDerivative) {
            _secondOrderDerivative = value;
            update();
        }
    }

    bool GLWidget::_createAllParametricCurvesAndTheirImages()
    {
        _pc.ResizeColumns(_pc_count);

        RowMatrix<ParametricCurve3::Derivative> derivative(3);

        derivative(0) = spiral_on_cone::d0;
        derivative(1) = spiral_on_cone::d1;
        derivative(2) = spiral_on_cone::d2;
        _pc[0] = new (nothrow) ParametricCurve3(derivative, spiral_on_cone::u_min, spiral_on_cone::u_max);
        if (!_pc[0])
        {
           _destroyAllExistingParametricCurvesAndTheirImages();
           return false;
        }

        // más paraméteres görbeobjektumok dinamikus létrehozása
        derivative(0) = circleCurve::d0;
        derivative(1) = circleCurve::d1;
        derivative(2) = circleCurve::d2;
        _pc[1] = new (nothrow) ParametricCurve3(derivative, circleCurve::u_min, circleCurve::u_max);
        if (!_pc[1])
        {
           _destroyAllExistingParametricCurvesAndTheirImages();
           return false;
        }

        derivative(0) = lissajousCurve::d0;
        derivative(1) = lissajousCurve::d1;
        derivative(2) = lissajousCurve::d2;
        _pc[2] = new (nothrow) ParametricCurve3(derivative, lissajousCurve::u_min, lissajousCurve::u_max);
        if (!_pc[2])
        {
           _destroyAllExistingParametricCurvesAndTheirImages();
           return false;
        }

        derivative(0) = butterflyCurve::d0;
        derivative(1) = butterflyCurve::d1;
        derivative(2) = butterflyCurve::d2;
        _pc[3] = new (nothrow) ParametricCurve3(derivative, butterflyCurve::u_min, butterflyCurve::u_max);
        if (!_pc[3])
        {
           _destroyAllExistingParametricCurvesAndTheirImages();
           return false;
        }

        derivative(0) = hyperParabolaCurve::d0;
        derivative(1) = hyperParabolaCurve::d1;
        derivative(2) = hyperParabolaCurve::d2;
        _pc[4] = new (nothrow) ParametricCurve3(derivative, hyperParabolaCurve::u_min, hyperParabolaCurve::u_max);
        if (!_pc[4])
        {
           _destroyAllExistingParametricCurvesAndTheirImages();
           return false;
        }

        derivative(0) = toruszCurve::d0;
        derivative(1) = toruszCurve::d1;
        derivative(2) = toruszCurve::d2;
        _pc[5] = new (nothrow) ParametricCurve3(derivative, toruszCurve::u_min, toruszCurve::u_max);
        if (!_pc[5])
        {
           _destroyAllExistingParametricCurvesAndTheirImages();
           return false;
        }

        derivative(0) = helixCurve::d0;
        derivative(1) = helixCurve::d1;
        derivative(2) = helixCurve::d2;
        _pc[6] = new (nothrow) ParametricCurve3(derivative, helixCurve::u_min, helixCurve::u_max);
        if (!_pc[6])
        {
           _destroyAllExistingParametricCurvesAndTheirImages();
           return false;
        }

        // a képek és azok csúcspont-pufferobjektumainak egységes létrehozása
        _image_of_pc.ResizeColumns(_pc_count);

        for (GLuint i = 0; i < _pc_count; i++)
        {
            _image_of_pc[i] = _pc[i]->GenerateImage(_div_point_count, _usage_flag);

            if (!_image_of_pc[i] || !_image_of_pc[i]->UpdateVertexBufferObjects(_scaleFirstOrderDerivative, _scaleSecondOrderDerivative, _usage_flag))
            {
                _destroyAllExistingParametricCurvesAndTheirImages();
                return false;
            }
        }

        return true;
    }

    void GLWidget::_destroyAllExistingParametricCurvesAndTheirImages()
    {
        for (GLuint i = 0; i < _pc_count; i++)
        {
             if (_pc[i])
             {
                 delete _pc[i]; _pc[i] = nullptr;
             }

             if (_image_of_pc[i])
             {
                 delete _image_of_pc[i]; _image_of_pc[i] = nullptr;
             }
        }
    }

    bool GLWidget::_renderSelectedParametricCurve(bool showTangent, bool showAcceleration)
    {
        if (!_image_of_pc[_selected_pc])
        {
             return false;
        }

        glColor3f(1.0f, 0.0f, 0.0f);
        _image_of_pc[_selected_pc]->RenderDerivatives(0, GL_LINE_STRIP); // ? EZ KELL IDE? GL_LINE_STRIP?

        if (showTangent)
        {
            glPointSize(5.0f);
            glColor3f(0.0f, 0.5f, 0.0f);
            _image_of_pc[_selected_pc]->RenderDerivatives(1, GL_LINES);
            _image_of_pc[_selected_pc]->RenderDerivatives(1, GL_POINTS);
        }

        if (showAcceleration)
        {
            glColor3f(0.1f, 0.5f, 0.9f);
            _image_of_pc[_selected_pc]->RenderDerivatives(2, GL_LINES);
            _image_of_pc[_selected_pc]->RenderDerivatives(2, GL_POINTS);
            glPointSize(1.0f);
        }

        return true;
    }

    bool GLWidget::_createAllParametricSurfacesAndTheirImages()
    {
        _ps.ResizeColumns(_ps_count);

        TriangularMatrix<ParametricSurface3::PartialDerivative> derivative(3);

        derivative(0, 0) = firstSurface::d00;
        derivative(1, 0) = firstSurface::d10;
        derivative(1, 1) = firstSurface::d01;
        _ps[0] = new (nothrow) ParametricSurface3(derivative, firstSurface::u_min, firstSurface::u_max,
                                                  firstSurface::v_min, firstSurface::v_max);
        if (!_ps[0])
        {
           _destroyAllExistingParametricSurfacesAndTheirImages();
           return false;
        }

        derivative(0, 0) = secondSurface::d00;
        derivative(1, 0) = secondSurface::d10;
        derivative(1, 1) = secondSurface::d01;
        _ps[1] = new (nothrow) ParametricSurface3(derivative, secondSurface::u_min, secondSurface::u_max,
                                                  secondSurface::v_min, secondSurface::v_max);
        if (!_ps[1])
        {
           _destroyAllExistingParametricSurfacesAndTheirImages();
           return false;
        }

        derivative(0, 0) = thirdSurface::d00;
        derivative(1, 0) = thirdSurface::d10;
        derivative(1, 1) = thirdSurface::d01;
        _ps[2] = new (nothrow) ParametricSurface3(derivative, thirdSurface::u_min, thirdSurface::u_max,
                                                  thirdSurface::v_min, thirdSurface::v_max);
        if (!_ps[2])
        {
           _destroyAllExistingParametricSurfacesAndTheirImages();
           return false;
        }

        derivative(0, 0) = forthSurface::d00;
        derivative(1, 0) = forthSurface::d10;
        derivative(1, 1) = forthSurface::d01;
        _ps[3] = new (nothrow) ParametricSurface3(derivative, forthSurface::u_min, forthSurface::u_max,
                                                  forthSurface::v_min, forthSurface::v_max);
        if (!_ps[3])
        {
           _destroyAllExistingParametricSurfacesAndTheirImages();
           return false;
        }

        derivative(0, 0) = fifthSurface::d00;
        derivative(1, 0) = fifthSurface::d10;
        derivative(1, 1) = fifthSurface::d01;
        _ps[4] = new (nothrow) ParametricSurface3(derivative, fifthSurface::u_min, fifthSurface::u_max,
                                                  fifthSurface::v_min, fifthSurface::v_max);
        if (!_ps[4])
        {
           _destroyAllExistingParametricSurfacesAndTheirImages();
           return false;
        }

        derivative(0, 0) = sixthSurface::d00;
        derivative(1, 0) = sixthSurface::d10;
        derivative(1, 1) = sixthSurface::d01;
        _ps[5] = new (nothrow) ParametricSurface3(derivative, sixthSurface::u_min, sixthSurface::u_max,
                                                  sixthSurface::v_min, sixthSurface::v_max);
        if (!_ps[5])
        {
           _destroyAllExistingParametricSurfacesAndTheirImages();
           return false;
        }

        // a képek és azok csúcspont-pufferobjektumainak egységes létrehozása
        _image_of_ps.ResizeColumns(_ps_count);

        for (GLuint i = 0; i < _ps_count; i++)
        {
            _image_of_ps[i] = _ps[i]->GenerateImage(_div_point_count_u, _div_point_count_v, _usage_flag);

            if (!_image_of_ps[i] || !_image_of_ps[i]->UpdateVertexBufferObjects(_usage_flag))
            {
                _destroyAllExistingParametricSurfacesAndTheirImages();
                return false;
            }
        }

        return true;
    }

    void GLWidget::_destroyAllExistingParametricSurfacesAndTheirImages()
    {
        for (GLuint i = 0; i < _ps_count; i++)
        {
             if (_ps[i])
             {
                 delete _ps[i]; _ps[i] = nullptr;
             }

             if (_image_of_ps[i])
             {
                 delete _image_of_ps[i]; _image_of_ps[i] = nullptr;
             }
        }
    }

    bool GLWidget::_renderSelectedParametricSurface()
    {
        if (!_image_of_ps[_selected_ps])
        {
             return false;
        }

        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_NORMALIZE);

        if (_display_textures)
        {
            glEnable(GL_TEXTURE_2D);
            _texture_list[_selected_texture]->bind();
        }

        MatFBBrass.Apply();
        _image_of_ps[_selected_ps]->Render();

        if (_display_normal_vectors)
        {
            glDisable(GL_LIGHTING);
            glColor3f(0.5f, 0.5f, 0.5f);
            _image_of_ps[_selected_ps]->RenderNormalVector(_scale_normals);
        }

        glDisable(GL_LIGHT0);
        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);

        return true;
    }

    // ------ //
    // 4. LAB //
    // ------ //
    bool GLWidget::_init_cyclic_curve()
    {
        GLuint _n = 2;
        _cc = new CyclicCurve3(_n);

        if (!_cc)
        {
          throw Exception("Could not create the cyclic curve!");
        }

        _dataPoints.ResizeRows(2 * _n + 1);
        GLdouble step = TWO_PI / (2 * _n + 1);
        for (GLuint i = 0; i < 2 * _n + 1; i++) {
          DCoordinate3& cp = (*_cc)[i];
          GLdouble u = i * step;
          cp[0] = cos(u);
          cp[1] = sin(u);
          cp[2] = -2.0 + 4.0 * (GLdouble)rand() / (GLdouble)RAND_MAX;
          _dataPoints[i] = cp;
        }

        if(!_cc->UpdateVertexBufferObjectsOfData())
        {
            throw Exception("Could not create the cyclic curve!");
        }

        _image_of_cc = _cc->GenerateImage(2, 100);
        if (!_image_of_cc) {
          throw Exception("Could not create the image of the cyclic curve!");
        }

        if (!_image_of_cc->UpdateVertexBufferObjects()) {
          throw Exception("Could not update vertex buffer objects!");
        }

        _knot_vector.ResizeRows(2 * _n + 1);
        _dataPointsToInterpolate.ResizeRows(2 * _n + 1);

        // Interpolation
        GLdouble val = 0.0;
        for (GLuint i = 0; i < 2 * _n + 1; i++) {
          _knot_vector(i) = val;
          val += step;
          _dataPointsToInterpolate(i) = (*_cc)[i];
        }

        if(!_cc->UpdateDataForInterpolation(_knot_vector, _dataPointsToInterpolate))
        {
            throw Exception("Could not create the interpolation!");
        }

        _image_of_cc_after = _cc->GenerateImage(2, 100);
        if (!_image_of_cc_after) {
          throw Exception("Could not create the image of the cyclic curve after interpolation!");
        }

        if (!_image_of_cc_after->UpdateVertexBufferObjects()) {
            throw Exception("Could not update vertex buffer objects after interpolation!");
        }

        emitCoords(_selected_cyclic_index);

        return true;
    }

    bool GLWidget::_render_cyclic_curve()
    {
        glPointSize(10.0f);
        glColor3f(1.0f, 1.0f, 0.0f);
        _cc->RenderData(GL_POINTS);
        if (!_image_of_cc->RenderDerivatives(0, GL_POINTS)) {
            return GL_FALSE;
        }
        glColor3f(1.0, 0.0, 0.0);
        _image_of_cc_after->RenderDerivatives(0, GL_LINE_LOOP);
        glBegin(GL_POINTS);
        for(GLuint i = 0; i < _dataPointsToInterpolate.GetRowCount(); i++)
        {
          glVertex3dv(&_dataPointsToInterpolate[i][0]);
        }
        glEnd();

        glPointSize(1.0f);

        glColor3f(0.0f, 0.8f, 1.0f);
        _image_of_cc->RenderDerivatives(1, GL_LINES);
        glColor3f(1.0f, 0.8f, 1.0f);
        _image_of_cc->RenderDerivatives(2, GL_LINES);
        glPointSize(1.0f);

        return GL_TRUE;
    }

    void GLWidget::emitCoords(int index)
    {
        _selected_cyclic_index = index;
        emit emitXCoord(_dataPoints[index][0]);
        emit emitYCoord(_dataPoints[index][1]);
        emit emitZCoord(_dataPoints[index][2]);
        emit emitXCoordInterp(_dataPointsToInterpolate[index][0]);
        emit emitYCoordInterp(_dataPointsToInterpolate[index][1]);
        emit emitZCoordInterp(_dataPointsToInterpolate[index][2]);
    }

    void GLWidget::setNewXCoord(double value)
    {
        _cc->DeleteVertexBufferObjectsOfData();
        _dataPoints[_selected_cyclic_index][0] = value;
        _updateCyclicCurve();
    }

    void GLWidget::setNewYCoord(double value)
    {
        _cc->DeleteVertexBufferObjectsOfData();
        _dataPoints[_selected_cyclic_index][1] = value;
        _updateCyclicCurve();
    }

    void GLWidget::setNewZCoord(double value)
    {
        _cc->DeleteVertexBufferObjectsOfData();
        _dataPoints[_selected_cyclic_index][2] = value;
        _updateCyclicCurve();
    }

    void GLWidget::_updateCyclicCurve()
    {
        GLuint _n = 2;
        for (GLuint i = 0; i < 2 * _n + 1; i++) {
          DCoordinate3& cp = (*_cc)[i];
          cp = _dataPoints[i];
        }

        if (!_cc->UpdateVertexBufferObjectsOfData())
        {
            throw Exception("Could not create the cyclic curve!");
        }

        _image_of_cc = _cc->GenerateImage(2, 100);
        if (!_image_of_cc) {
          throw Exception("Could not create the image of the cyclic curve!");
        }

        if (!_image_of_cc->UpdateVertexBufferObjects()) {
          throw Exception("Could not update vertex buffer objects!");
        }

        update();
    }

    void GLWidget::setNewXCoordInterp(double value)
    {
        _dataPointsToInterpolate[_selected_cyclic_index][0] = value;
        _updateCyclicCurveInterp();
    }

    void GLWidget::setNewYCoordInterp(double value)
    {
        _dataPointsToInterpolate[_selected_cyclic_index][1] = value;
        _updateCyclicCurveInterp();
    }

    void GLWidget::setNewZCoordInterp(double value)
    {
        _dataPointsToInterpolate[_selected_cyclic_index][2] = value;
        _updateCyclicCurveInterp();
    }

    void GLWidget::_updateCyclicCurveInterp()
    {
        GLuint _n = 2;
        for (GLuint i = 0; i < 2 * _n + 1; i++) {
          DCoordinate3& cp = (*_cc)[i];
          cp = _dataPointsToInterpolate[i];
        }

        if(!_cc->UpdateDataForInterpolation(_knot_vector, _dataPointsToInterpolate))
        {
            throw Exception("Could not create the interpolation!");
        }

        _image_of_cc_after = _cc->GenerateImage(2, 100);
        if (!_image_of_cc_after) {
          throw Exception("Could not create the image of the cyclic curve after interpolation!");
        }

        if (!_image_of_cc_after->UpdateVertexBufferObjects()) {
            throw Exception("Could not update vertex buffer objects after interpolation!");
        }

        update();
    }

    // ------ //
    // 4.2. LAB //
    // ------ //
    bool GLWidget::_init_biquartic()
    {
        _cc_biquartic = new BiquarticArcs3();

        if (!_cc_biquartic)
        {
            throw Exception("Could not create the biquartic arc!");
        }

        (*_cc_biquartic)[0] = DCoordinate3(0, 0, 0);
        (*_cc_biquartic)[1] = DCoordinate3(1, 1, 0);
        (*_cc_biquartic)[2] = DCoordinate3(1, 2, 0);
        (*_cc_biquartic)[3] = DCoordinate3(0, 3, 0);


        if(!_cc_biquartic->UpdateVertexBufferObjectsOfData())
        {
            throw Exception("Could not create the biquartic arc!");
        }

        _image_of_cc_biquartic = _cc_biquartic->GenerateImage(2, 100);
        if (!_image_of_cc_biquartic) {
            throw Exception("Could not create the image of the biquartic arc!");
        }

        if (!_image_of_cc_biquartic->UpdateVertexBufferObjects()) {
            throw Exception("Could not update vertex buffer objects!");
        }

        return GL_TRUE;
    }

    bool GLWidget::_render_biquartic()
    {
        if (!_image_of_cc_biquartic)
        {
            return GL_FALSE;
        }

        glColor3f(1.0, 0.8, 0.0);
        _image_of_cc_biquartic->RenderDerivatives(0, GL_LINE_STRIP);

        _cc_biquartic->RenderData();

        if (_biquatricFirstOrderDerivative)
        {
            glColor3f(0.0f, 0.8f, 1.0f);
            _image_of_cc_biquartic->RenderDerivatives(1, GL_LINES);
        }

        if (_biquatricSecondOrderDerivative)
        {
            glColor3f(1.0f, 0.8f, 1.0f);
            _image_of_cc_biquartic->RenderDerivatives(2, GL_LINES);
        }

        return GL_TRUE;
    }

    // ------ //
    // 4.3. LAB //
    // ------ //
    bool GLWidget::_init_patch()
    {
        _patch = new BiquarticPatch3();

        _patch->SetData(0, 0, -2.0, -2.0, 0.0);
        _patch->SetData(0, 1, -2.0, -1.0, 0.0);
        _patch->SetData(0, 2, -2.0, 1.0, 0.0);
        _patch->SetData(0, 3, -2.0, 2.0, 0.0);

        _patch->SetData(1, 0, -1.0, -2.0, 0.0);
        _patch->SetData(1, 1, -1.0, -1.0, 2.0);
        _patch->SetData(1, 2, -1.0, 1.0, 2.0);
        _patch->SetData(1, 3, -1.0, 2.0, 0.0);

        _patch->SetData(2, 0, 1.0, -2.0, 0.0);
        _patch->SetData(2, 1, 1.0, -1.0, 2.0);
        _patch->SetData(2, 2, 1.0, 1.0, 2.0);
        _patch->SetData(2, 3, 1.0, 2.0, 0.0);

        _patch->SetData(3, 0, 2.0, -2.0, 0.0);
        _patch->SetData(3, 1, 2.0, -1.0, 0.0);
        _patch->SetData(3, 2, 2.0, 1.0, 0.0);
        _patch->SetData(3, 3, 2.0, 2.0, 0.0);

        _before_int = _patch->GenerateImage(30, 30, GL_STATIC_DRAW);

        if (_before_int)
            _before_int->UpdateVertexBufferObjects();

        RowMatrix<GLdouble> u_knot_vector(4);
        ColumnMatrix<GLdouble> v_knot_vector(4);
        u_knot_vector(0) = v_knot_vector(0) = 0.0;
        u_knot_vector(1) = v_knot_vector(1) = 1.0 / 3.0;
        u_knot_vector(2) = v_knot_vector(2) = 2.0 / 3.0;
        u_knot_vector(3) = v_knot_vector(3) = 1.0;

        Matrix<DCoordinate3> data_points_to_interpolate(4, 4);
        for (GLuint row = 0; row < 4; ++row)
            for (GLuint column = 0; column < 4; ++column)
                _patch->GetData(row, column, data_points_to_interpolate(row, column));

       if (_patch->UpdateDataForInterpolation(u_knot_vector, v_knot_vector, data_points_to_interpolate))
       {
           _after_int = _patch->GenerateImage(30, 30, GL_STATIC_DRAW);

           if (_after_int)
               _after_int->UpdateVertexBufferObjects();
       }

       _u_lines = _patch->GenerateUIsoparametricLines(40, 1, 30);
       _v_lines = _patch->GenerateVIsoparametricLines(10, 1, 30);

       for(GLuint i = 0; i < 40; i++){
           (*_u_lines)[i]->UpdateVertexBufferObjects(0.1);
       }

       for(GLuint i = 0; i < 10; i++){
           (*_v_lines)[i]->UpdateVertexBufferObjects(0.1);
       }

       return GL_TRUE;
    }

    bool GLWidget::_render_patch()
    {
        glDisable(GL_LIGHTING);
        glColor3f(1.0f,1.0f,1.0f);

        glEnable(GL_LIGHTING);

        if (_before_int)
        {
            MatFBRuby.Apply();
            _before_int->Render();
        }


        if (_after_int)
        {
            glEnable(GL_BLEND);
            glDepthMask(GL_FALSE);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                MatFBTurquoise.Apply();
                _after_int->Render();
            glDepthMask(GL_TRUE);
            glDisable(GL_BLEND);
        }

        glDisable(GL_LIGHTING);

        if (_biquatricFirstOrderDerivative)
        {
            glColor3d(0.0, 1.0, 0.0);
            for(GLuint i = 0; i < 40; i++){
                (*_u_lines)[i]->RenderDerivatives(0,GL_LINE_STRIP);
                (*_u_lines)[i]->RenderDerivatives(1,GL_LINES);
            }
        }

        if (_biquatricSecondOrderDerivative)
        {
            glColor3d(0.0, 0.0, 1.0);
            for(GLuint i = 0; i < 10; i++){
                (*_v_lines)[i]->RenderDerivatives(0,GL_LINE_STRIP);
                (*_v_lines)[i]->RenderDerivatives(1,GL_LINES);
            }
        }


        return GL_TRUE;
    }

    void GLWidget::biquatricShowFirstOrderDerivative(int value)
    {
        if (value != _biquatricFirstOrderDerivative)
        {
            _biquatricFirstOrderDerivative = value;
            update();
        }
    }

    void GLWidget::biquatricShowSecondOrderDerivative(int value)
    {
        if (value != _biquatricSecondOrderDerivative)
        {
            _biquatricSecondOrderDerivative = value;
            update();
        }
    }

    void GLWidget::renderSampleTriangle()
    {
        glColor3f(1.0f, 1.0f, 1.0f);
        glBegin(GL_LINES);
            glVertex3f(0.0f, 0.0f, 0.0f);
            glVertex3f(1.1f, 0.0f, 0.0f);

            glVertex3f(0.0f, 0.0f, 0.0f);
            glVertex3f(0.0f, 1.1f, 0.0f);

            glVertex3f(0.0f, 0.0f, 0.0f);
            glVertex3f(0.0f, 0.0f, 1.1f);
        glEnd();

        glBegin(GL_TRIANGLES);
            // attributes
            glColor3f(1.0f, 0.0f, 0.0f);
            // associated with position
            glVertex3f(1.0f, 0.0f, 0.0f);

            // attributes
            glColor3f(0.0, 1.0, 0.0);
            // associated with position
            glVertex3f(0.0, 1.0, 0.0);

            // attributes
            glColor3f(0.0f, 0.0f, 1.0f);
            // associated with position
            glVertex3f(0.0f, 0.0f, 1.0f);
        glEnd();
    }

    void GLWidget::_animate()
    {
        GLfloat *vertex = _mouse.MapVertexBuffer(GL_READ_WRITE);
        GLfloat *normal = _mouse.MapNormalBuffer(GL_READ_ONLY);

        _angle += DEG_TO_RADIAN;
        if (_angle >= TWO_PI) _angle -= TWO_PI;

        GLfloat scale = sin(_angle) / 3000.0;
        for (GLuint i = 0; i < _mouse.VertexCount(); i++) {
            for (GLuint coordinate = 0; coordinate < 3; ++coordinate, ++vertex, ++normal)
                *vertex += scale * (*normal);
        }

        _mouse.UnmapVertexBuffer();
        _mouse.UnmapNormalBuffer();

        update();
    }

    bool GLWidget::_init_castle()
    {
        _model_count = 5;
        _model.ResizeColumns(_model_count);
        if (!_model[0].LoadFromOFF("models/cube.off", GL_TRUE) ||
            !_model[1].LoadFromOFF("models/cone.off", GL_TRUE) ||
            !_model[2].LoadFromOFF("models/gangster.off", GL_TRUE) ||
            !_model[3].LoadFromOFF("models/dragon.off", GL_TRUE) ||
            !_model[4].LoadFromOFF("models/Lucy.off", GL_TRUE)) {
            throw Exception("Could not load model file!");
        }
        if (!_model[0].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW) ||
            !_model[1].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW) ||
            !_model[2].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW) ||
            !_model[3].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW) ||
            !_model[4].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)) {
            throw Exception("Could not update the vertex buffer objects of the triangulated mesh!");
        }

        ifstream in("castle/coords.txt", ios_base::in);
        in >> _castle_size;
        _castle.ResizeColumns(_castle_size);

        for (GLuint i = 0; i < _castle_size; i++)
        {
            in >> _castle[i];
        }

        return true;
    }

    bool GLWidget::_render_castle()
    {
        for (GLuint i = 0; i < _castle.GetColumnCount(); i++)
        {
            const ModelProperties &mp = _castle[i]; // constant reference to i-th model properties

            glPushMatrix();
                glEnable(GL_LIGHTING);
                glEnable(GL_LIGHT0);
                glEnable(GL_NORMALIZE);

                glTranslated(mp.position[0], mp.position[1], mp.position[2]);
                glScaled(mp.scale[0], mp.scale[1], mp.scale[2]);
                glRotated(mp.angle[0], 1.0, 0.0, 0.0);
                glRotated(mp.angle[1], 0.0, 1.0, 0.0);
                glRotated(mp.angle[2], 0.0, 0.0, 1.0);

                if (!mp.id)
                    MatFBBrass.Apply();
                else if (mp.id == 1 || mp.id == 3)
                    MatFBRuby.Apply();
                else if (mp.id == 4)
                    MatFBPearl.Apply();
                else
                    MatFBEmerald.Apply();

                _model[mp.id].Render();

                glEnable(GL_NORMALIZE);
                glEnable(GL_LIGHT0);
                glEnable(GL_LIGHTING);

            glPopMatrix();
        }

        return true;
    }

    // ------ //
    // 5. LAB //
    // ------ //
    void GLWidget::_loadSomeModels()
    {
        _model_count = 5;
        _model.ResizeColumns(_model_count);
        if (!_model[0].LoadFromOFF("models/cube.off", GL_TRUE) ||
            !_model[1].LoadFromOFF("models/cone.off", GL_TRUE) ||
            !_model[2].LoadFromOFF("models/gangster.off", GL_TRUE) ||
            !_model[3].LoadFromOFF("models/dragon.off", GL_TRUE) ||
            !_model[4].LoadFromOFF("models/Lucy.off", GL_TRUE)) {
            throw Exception("Could not load model file!");
        }
        if (!_model[0].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW) ||
            !_model[1].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW) ||
            !_model[2].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW) ||
            !_model[3].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW) ||
            !_model[4].UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)) {
            throw Exception("Could not update the vertex buffer objects of the triangulated mesh!");
        }
    }

    void GLWidget::_installShaders()
    {
        for (GLuint i = 0; i < _shader_count; i++)
        {
            _shaders[i] = nullptr;
        }

        _shaders[0] = new ShaderProgram();
        if (!_shaders[0]->InstallShaders("shaders/directional_light.vert", "shaders/directional_light.frag"))
            throw Exception("Could not install directional light shaders!");
        _shaders[1] = new ShaderProgram();
        if (!_shaders[1]->InstallShaders("shaders/two_sided_lighting.vert", "shaders/two_sided_lighting.frag"))
            throw Exception("Could not install two sided directional light shaders!");
        _shaders[2] = new ShaderProgram();
        if (!_shaders[2]->InstallShaders("shaders/toon.vert", "shaders/toon.frag"))
            throw Exception("Could not install toonifying shaders!");
        _shaders[3] = new ShaderProgram();
        if (!_shaders[3]->InstallShaders("shaders/reflection_lines.vert", "shaders/reflection_lines.frag"))
            throw Exception("Could not install reflection line shaders!");
    }

    void GLWidget::_renderSpotWithShaders()
    {
        glEnable(GL_LIGHTING);
        _shaders[_selected_shader]->Enable();
        if (_selected_shader == 2)
        {
            if (!_shaders[2]->SetUniformVariable4fv("default_outline_color", 1, _toon_def_color))
                throw Exception("Could not set default_outline_color for toonifying shader!");
        }
        else if (_selected_shader == 3)
        {
            if (!_shaders[3]->SetUniformVariable1f("scale_factor", _reflection_lines_unif_params[0]))
                throw Exception("Could not set scale_factor for reflection lines shader!");
            if (!_shaders[3]->SetUniformVariable1f("smoothing", _reflection_lines_unif_params[1]))
                throw Exception("Could not set smoothing for reflection lines shader!");
            if (!_shaders[3]->SetUniformVariable1f("shading", _reflection_lines_unif_params[2]))
                throw Exception("Could not set shading for reflection lines shader!");
        }

        if (_alpha_on)
        {
            glEnable(GL_BLEND);
            glDepthMask(GL_FALSE);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE);
            MatFBBrass.Apply();
            _model[3].Render();
            glDepthMask(GL_TRUE);
            glDisable(GL_BLEND);
        }
        else
        {
            MatFBBrass.Apply();
            _model[3].Render();
        }
        _shaders[_selected_shader]->Disable();
        glDisable(GL_LIGHTING);
    }

    void GLWidget::_destroyShaders()
    {
        for (GLuint i = 0; i < _shader_count; i++)
        {
            if (_shaders[i])
            {
                delete _shaders[i];
                _shaders[i] = nullptr;
            }
        }
    }

    void GLWidget::setSelectedShader(int value)
    {
        _selected_shader = value;
        update();
    }


    void GLWidget::setSelectedShaderScale(double value)
    {
        if (_reflection_lines_unif_params[0] != value)
        {
            _reflection_lines_unif_params[0] = value;
            if (_selected_shader == 3)
                update();
        }
    }

    void GLWidget::setSelectedShaderSmooth(double value)
    {
        if (_reflection_lines_unif_params[1] != value)
        {
            _reflection_lines_unif_params[1] = value;
            if (_selected_shader == 3)
                update();
        }
    }

    void GLWidget::setSelectedShaderShading(double value)
    {
        if (_reflection_lines_unif_params[2] != value)
        {
            _reflection_lines_unif_params[2] = value;
            if (_selected_shader == 3)
                update();
        }
    }

    //----------------------------------
    // your simple private test function
    //----------------------------------
    void GLWidget::_privateTestFunction()
    {
        // create log-files for your test-cases
        // the output files will be generated inside the folder build-QtFramework-...-{Debug|Release}
        fstream log("output.txt", ios_base::out);
        fstream input("input.txt", ios_base::in);

        log << "Testing vector- or matrix-related operations..." << endl;
        log << "-----------------------------------------------" << endl;

        /*DCoordinate3 i(1, 0, 0), j(0, 1, 0);
        DCoordinate3 k = i^j;                // the result should be (0, 0, 1)
        log << "testing cross product: " << k << endl;

        log << "testing the special constructor of the template class Matrix";
        Matrix<int> M(3, 5);
        log << M << endl;

        RowMatrix<int> RM(5);
        log << RM << endl;
        RM.ResizeRows(10);
        log << RM << endl;*/

        DCoordinate3 v;
        DCoordinate3 v2(4, 5, 6);

        Matrix<int>  M;

        input >> v >> M;

//        log << v << endl;
//        log << v.x() << ' ' << v.y() << ' ' << v.z() << endl;
//        log << v2 << endl;
//        log << (v * v2);

//        Matrix<int> N(M);
        Matrix<int> N;
        N = M;
        log << N << endl << endl;

//        N.ResizeRows(4);
//        N.ResizeColumns(8);
//        ColumnMatrix<int> cm(3);
//        N.SetColumn(0, cm);
        TriangularMatrix<int> tm(3);
//        RealSquareMatrix rqm(4);
        log << tm << endl << endl;
//        RealSquareMatrix rqm2(rqm);
//        log << rqm2 << endl << endl;
//        rqm.ResizeColumns(2);
//        rqm.ResizeRows(2);



//        int element_ref = N(1, 1);
//        log << element_ref << endl;

        input.close();

        // if you detect errors, throw some meaningful exceptions, which will be catched
        // by the try-catch-block that appears in the method GLWidget::initializeGL()
        // if (/*error*/)
        // {
        //    throw Exception("Could not perform the ... operation!");
        // }

        // etc.

        // you should also test ALL input stream operators!

        log.close();
    }
}
