#pragma once

#include <GL/glew.h>
#include <QOpenGLWidget>
#include <QOpenGLTexture>
#include "../Core/Materials.h"
#include "../Parametric/ParametricCurves3.h"
#include "../Parametric/ParametricSurfaces3.h"
#include "../Test/TestFunctions.h"
#include "../Core/TriangulatedMeshes3.h"
#include "../Core/Lights.h"
#include "../Cyclic/CyclicCurves3.h"
#include "../Projekt/biquarticArcs3.h"
#include "../Projekt/biquarticpatch3.h"
#include "../Core/ShaderPrograms.h"

namespace cagd
{
    class GLWidget: public QOpenGLWidget
    {
        Q_OBJECT

    private:

        // variables defining the projection matrix
        double       _aspect;            // aspect ratio of the rendering window
        double       _fovy;              // field of view in direction y
        double       _z_near, _z_far;    // distance of near and far clipping planes

        // variables defining the model-view matrix
        double       _eye[3], _center[3], _up[3];

        // variables needed by transformations
        int         _angle_x, _angle_y, _angle_z;
        double      _zoom;
        double      _trans_x, _trans_y, _trans_z;

        // your other declarations
        // a number that can be used to switch between different test-cases
        int  _homework_id = 5;

        // a simple test-function that does not need dynamical memory allocation, e.g.,
        // _testingDCoordinates(), _testingTemplateMatrices(), etc.
        void renderSampleTriangle();

        // ------ //
        // 1. LAB //
        // ------ //
        void _privateTestFunction();

        // ------ //
        // 2. LAB //
        // ------ //
        GLuint                       _pc_count = 7;
        RowMatrix<ParametricCurve3*> _pc;
        RowMatrix<GenericCurve3*>    _image_of_pc;
        GLenum                       _usage_flag = GL_STATIC_DRAW;
        GLuint                       _div_point_count = 200;
        int                          _selected_pc = 0;
//        bool                         _show_tangents = false; // most melyiket kene hasznalni? int?
//        bool                         _show_acceleration_vectors = false;
        int                          _firstOrderDerivative = 0;
        int                          _secondOrderDerivative = 0;
        double                       _scaleFirstOrderDerivative = 1.0;
        double                       _scaleSecondOrderDerivative = 1.0;

        bool _createAllParametricCurvesAndTheirImages();
        void _destroyAllExistingParametricCurvesAndTheirImages();
        bool _renderSelectedParametricCurve(bool showTangent = false, bool showAcceleration = false);
        bool customScaleParametricCurves();

        // ------ //
        // 3. LAB //
        // ------ //
        QTimer *_timer;
        GLfloat _angle;

        TriangulatedMesh3 _mouse;
        DirectionalLight *_dl = nullptr;

        GLuint                         _display_textures = 0;
        GLuint                         _texture_count = 10;
        GLuint                         _selected_texture = 0;
        RowMatrix<QOpenGLTexture*>     _texture_list;

        GLuint                         _ps_count = 6;
        RowMatrix<ParametricSurface3*> _ps;
        RowMatrix<TriangulatedMesh3*>  _image_of_ps;
        GLuint                         _div_point_count_u = 200;
        GLuint                         _div_point_count_v = 200;
        GLuint                         _display_normal_vectors = 0;
        GLdouble                       _scale_normals = 1.0;
        int                            _selected_ps = 0;
        bool _createAllParametricSurfacesAndTheirImages();
        void _destroyAllExistingParametricSurfacesAndTheirImages();
        bool _renderSelectedParametricSurface();

        // START - Declare the castle related objects
        GLint                          _model_count = 0;
        RowMatrix<TriangulatedMesh3>   _model;

        struct ModelProperties
        {
            GLuint       id;        // identifies a model, i.e., _model[id]
            DCoordinate3 position;  // new position of the middle point of the model's bounding box
            GLdouble     angle[3];  // rotational angles
            GLdouble     scale[3];  // scaling parameters

            friend std::istream& operator >>(std::istream& lhs, ModelProperties& rhs)
            {
                lhs >> rhs.id >> rhs.position >>
                       rhs.angle[0] >> rhs.angle[1] >> rhs.angle[2] >>
                       rhs.scale[0] >> rhs.scale[1] >> rhs.scale[2];
                return lhs;
            }

            friend std::ostream& operator <<(std::ostream& lhs, ModelProperties& rhs)
            {
                lhs << rhs.id << " " << rhs.position << " " <<
                       rhs.angle[0] << " " << rhs.angle[1] << " " << rhs.angle[2] <<
                       rhs.scale[0] << " " << rhs.scale[1] << " " << rhs.scale[2] << std::endl;
                return lhs;
            }
        };

        bool _init_castle();
        bool _render_castle();

        GLuint                     _castle_size = 0;
        RowMatrix<ModelProperties> _castle;
        // END - Declare the castle related objects

        // ------ //
        // 4. LAB //
        // ------ //
        CyclicCurve3*  _cc = nullptr;
        GenericCurve3* _image_of_cc;
        GenericCurve3* _image_of_cc_after;
        ColumnMatrix<GLdouble> _knot_vector;
        ColumnMatrix<DCoordinate3> _dataPoints;
        ColumnMatrix<DCoordinate3> _dataPointsToInterpolate;

        bool _init_cyclic_curve();
        bool _render_cyclic_curve();

        int _selected_cyclic_index = 0;
        void _updateCyclicCurve();
        void _updateCyclicCurveInterp();

        // ------ //
        // 4.2. LAB //
        // ------ //
        BiquarticArcs3*  _cc_biquartic = nullptr;
        GenericCurve3* _image_of_cc_biquartic;

        bool _init_biquartic();
        bool _render_biquartic();

        // ------ //
        // 4.3. LAB //
        // ------ //
        BiquarticPatch3*  _patch;
        TriangulatedMesh3 *_before_int, *_after_int;
        RowMatrix<GenericCurve3*> *_u_lines, *_v_lines;

        bool _init_patch();
        bool _render_patch();

        int _biquatricFirstOrderDerivative = 0;
        int _biquatricSecondOrderDerivative = 0;

        // ------ //
        // 5. LAB //
        // ------ //
        GLuint _shader_count = 4;
        GLuint _selected_shader = 1;
        RowMatrix<ShaderProgram*> _shaders = RowMatrix<ShaderProgram*>(_shader_count);
        GLfloat         _toon_def_color[4] = {0.0f, 0.0f, 0.0f, 0.5f};
        int             _alpha_on = 0;
        GLfloat         _reflection_lines_unif_params[3] = {5.0f, 2.0f, 1.0f};
        void _loadSomeModels();
        void _installShaders();
        void _renderSpotWithShaders();
        void _destroyShaders();

    public:
        // special and default constructor
        // the format specifies the properties of the rendering window
        GLWidget(QWidget* parent = 0);

        // redeclared virtual functions
        void initializeGL();
        void paintGL();
        void resizeGL(int w, int h);

        // destructor that frees all dynamically allocated memory objects
        ~GLWidget();

    private slots:
        // it is used by the GLWidget in order to accept/handle its own signals, e.g.,
        // in the constructor of GLWidget one can define a new private connection:
        // connect(this, SIGNAL(signal_name(/*formal type-list*/)),
        //         this, SLOT(_privateEventHandler(/*either empty or the same formal type-list*/)));
        //void _privateEventHandler(/*formal parameterlist that is either empty,
                                    //or coincides with the type-list signature of
                                    //the signal the has to be accpeted*/);
        void _animate();

    public slots:
        // public event handling methods/slots
        void set_angle_x(int value);
        void set_angle_y(int value);
        void set_angle_z(int value);

        void set_zoom_factor(double value);

        void set_trans_x(double value);
        void set_trans_y(double value);
        void set_trans_z(double value);

        // ------ //
        // 2. LAB //
        // ------ //
        void setSelectedCurve(int value);
        void setPointCount(int value);
        void setScaleFirstOrderDerivative(double value);
        void setScaleSecondOrderDerivative(double value);
        void showFirstOrderDerivative(int value);
        void showSecondOrderDerivative(int value);

        // ------ //
        // 3. LAB //
        // ------ //
        void setSelectedSurface(int value);
        void setPointCount_u(int value);
        void setPointCount_v(int value);
        void showTextures(int value);
        void setSelectedTexture(int value);
        void showNormalVectors(int value);
        void setScaleNormalVectors(double value);

        // ------ //
        // 4. LAB //
        // ------ //
        void emitCoords(int);
        void setNewXCoord(double);
        void setNewYCoord(double);
        void setNewZCoord(double);
        void setNewXCoordInterp(double);
        void setNewYCoordInterp(double);
        void setNewZCoordInterp(double);

        // ------ //
        //   BIQ  //
        // ------ //
        void biquatricShowFirstOrderDerivative(int value);
        void biquatricShowSecondOrderDerivative(int value);

        // ------ //
        // 5. LAB //
        // ------ //
        void setSelectedShader(int index);
        void setSelectedShaderScale(double value);
        void setSelectedShaderSmooth(double value);
        void setSelectedShaderShading(double value);

        // it is used by the GLWidget in order to accept/handle the signals of other widgets,
        // e.g., in the constructor of the MainWindow one can introduce a new connection:
        // connect(address_of_another_widget, SIGNAL(signal_name(/*formal type-list*/)),
        //         _gl_widget, SLOT(publicEnventHandler(/*either empty or the same formal type-list*/)));
        //void publicEventHandler(/*formal parameterlist that is either empty,
                                  //or coincides with the type-list signature of
                                  //the signal the has to be accpeted*/);
//        void setParametricCurveIndex(int index);
//        void setVisibilityOfTangents(bool visibility);
//        void setVisibilityOfAccelerationVectors(bool visibility);

    signals:
        // if required, one can declare new signals for the GLWidget, that can be emitted
        // for other widgets, by using the command
        //
        // emit somethingHasChanged(/*values of parameters that follow the formal type-list*/);
        //
        // in a GLWidget-related method...
        //
        // then in the constructor of the MainWindow one can define a new connection:
        // connect(_gl_widget,
        //          SIGNAL(somethingHasChanged(/*formal type-list*/)),
        //         _side_widget->name_of_child_widget,
        //          SLOT(publicEnventHandler(/*either empty or the same formal type-list*/)));
        void somethingHasChanged(/*formal type-list*/);
        void emitXCoord(double);
        void emitYCoord(double);
        void emitZCoord(double);
        void emitXCoordInterp(double);
        void emitYCoordInterp(double);
        void emitZCoordInterp(double);
    };
}
