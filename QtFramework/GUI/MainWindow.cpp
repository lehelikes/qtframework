#include "MainWindow.h"

namespace cagd
{
    MainWindow::MainWindow(QWidget *parent): QMainWindow(parent)
    {
        setupUi(this);

    /*

      the structure of the main window's central widget

     *---------------------------------------------------*
     |                 central widget                    |
     |                                                   |
     |  *---------------------------*-----------------*  |
     |  |     rendering context     |   scroll area   |  |
     |  |       OpenGL widget       | *-------------* |  |
     |  |                           | | side widget | |  |
     |  |                           | |             | |  |
     |  |                           | |             | |  |
     |  |                           | *-------------* |  |
     |  *---------------------------*-----------------*  |
     |                                                   |
     *---------------------------------------------------*

    */
        _side_widget = new SideWidget(this);

        _scroll_area = new QScrollArea(this);
        _scroll_area->setWidget(_side_widget);
        _scroll_area->setSizePolicy(_side_widget->sizePolicy());
        _scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

        _gl_widget = new GLWidget(this);

        centralWidget()->setLayout(new QHBoxLayout());
        centralWidget()->layout()->addWidget(_gl_widget);
        centralWidget()->layout()->addWidget(_scroll_area);

        // creating a signal slot mechanism between the rendering context and the side widget
        connect(_side_widget->rotate_x_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));

        connect(_side_widget->zoom_factor_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

        connect(_side_widget->trans_x_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_x(double)));
        connect(_side_widget->trans_y_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_y(double)));
        connect(_side_widget->trans_z_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_z(double)));

        connect(_side_widget->selectCurve, SIGNAL(valueChanged(int)), _gl_widget, SLOT(setSelectedCurve(int)));
        connect(_side_widget->selectPointCount, SIGNAL(valueChanged(int)), _gl_widget, SLOT(setPointCount(int)));
        connect(_side_widget->scaleFirstOrderDerivative, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setScaleFirstOrderDerivative(double)));
        connect(_side_widget->scaleSecondOrderDerivative, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setScaleSecondOrderDerivative(double)));
        connect(_side_widget->checkBoxFirstOrderDerivative, SIGNAL(stateChanged(int)), _gl_widget, SLOT(showFirstOrderDerivative(int)));
        connect(_side_widget->checkBoxSecondOrderDerivative, SIGNAL(stateChanged(int)), _gl_widget, SLOT(showSecondOrderDerivative(int)));

        connect(_side_widget->selectSurface, SIGNAL(valueChanged(int)), _gl_widget, SLOT(setSelectedSurface(int)));
        connect(_side_widget->selectPointCount_u, SIGNAL(valueChanged(int)), _gl_widget, SLOT(setPointCount_u(int)));
        connect(_side_widget->selectPointCount_v, SIGNAL(valueChanged(int)), _gl_widget, SLOT(setPointCount_v(int)));
        connect(_side_widget->checkBoxShowTextures, SIGNAL(stateChanged(int)), _gl_widget, SLOT(showTextures(int)));
        connect(_side_widget->selectTexture, SIGNAL(valueChanged(int)), _gl_widget, SLOT(setSelectedTexture(int)));
        connect(_side_widget->checkBoxShowNormalVectors, SIGNAL(stateChanged(int)), _gl_widget, SLOT(showNormalVectors(int)));
        connect(_side_widget->scaleNormalVectors, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setScaleNormalVectors(double)));

        // Lab 4
        // GL to UI
        connect(_gl_widget, SIGNAL(emitXCoord(double)), _side_widget->changeXCoord, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(emitYCoord(double)), _side_widget->changeYCoord, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(emitZCoord(double)), _side_widget->changeZCoord, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(emitXCoordInterp(double)), _side_widget->changeXCoordInterp, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(emitYCoordInterp(double)), _side_widget->changeYCoordInterp, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(emitZCoordInterp(double)), _side_widget->changeZCoordInterp, SLOT(setValue(double)));

        // UI to GL
        connect(_side_widget->selectCPIndex, SIGNAL(valueChanged(int)), _gl_widget, SLOT(emitCoords(int)));
        connect(_side_widget->changeXCoord, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setNewXCoord(double)));
        connect(_side_widget->changeYCoord, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setNewYCoord(double)));
        connect(_side_widget->changeZCoord, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setNewZCoord(double)));
        connect(_side_widget->changeXCoordInterp, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setNewXCoordInterp(double)));
        connect(_side_widget->changeYCoordInterp, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setNewYCoordInterp(double)));
        connect(_side_widget->changeZCoordInterp, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setNewZCoordInterp(double)));

        // Biquatric        
        connect(_side_widget->biqShow1stOrder, SIGNAL(stateChanged(int)), _gl_widget, SLOT(biquatricShowFirstOrderDerivative(int)));
        connect(_side_widget->biqShow2ndOrder, SIGNAL(stateChanged(int)), _gl_widget, SLOT(biquatricShowSecondOrderDerivative(int)));

        // Lab 5
        connect(_side_widget->selectShader, SIGNAL(valueChanged(int)), _gl_widget, SLOT(setSelectedShader(int)));
        connect(_side_widget->scaleShader, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setSelectedShaderScale(double)));
        connect(_side_widget->smoothShader, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setSelectedShaderSmooth(double)));
        connect(_side_widget->shadingShader, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setSelectedShaderShading(double)));
    }

    //--------------------------------
    // implementation of private slots
    //--------------------------------
    void MainWindow::on_action_Quit_triggered()
    {
        qApp->exit(0);
    }
}
