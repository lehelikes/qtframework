#pragma once

#include "../Core/DCoordinates3.h"
#include "../Core/Constants.h"

namespace cagd
{
    namespace spiral_on_cone
    {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace circleCurve {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace lissajousCurve {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace butterflyCurve {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace hyperParabolaCurve {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    // Below for surface matching
    namespace toruszCurve {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace helixCurve {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    //
    // PARAMETRIC SURFACES
    //

    namespace firstSurface {
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble u, GLdouble v);
        DCoordinate3 d01(GLdouble u, GLdouble v);
        DCoordinate3 d10(GLdouble u, GLdouble v);
    }

    namespace secondSurface {
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble u, GLdouble v);
        DCoordinate3 d01(GLdouble u, GLdouble v);
        DCoordinate3 d10(GLdouble u, GLdouble v);
    }

    namespace thirdSurface {
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble, GLdouble);
        DCoordinate3 d10(GLdouble, GLdouble);
        DCoordinate3 d01(GLdouble, GLdouble);
    }

    namespace forthSurface {
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble, GLdouble);
        DCoordinate3 d10(GLdouble, GLdouble);
        DCoordinate3 d01(GLdouble, GLdouble);

    }

    namespace fifthSurface {
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble, GLdouble);
        DCoordinate3 d10(GLdouble, GLdouble);
        DCoordinate3 d01(GLdouble, GLdouble);
    }

    namespace sixthSurface {
        extern GLdouble u_min, u_max,
                        v_min, v_max;

        DCoordinate3 d00(GLdouble, GLdouble);
        DCoordinate3 d10(GLdouble, GLdouble);
        DCoordinate3 d01(GLdouble, GLdouble);
    }
}
