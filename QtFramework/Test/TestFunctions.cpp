#include <cmath>
#include "TestFunctions.h"

using namespace cagd;
using namespace std;

GLdouble spiral_on_cone::u_min = -TWO_PI;
GLdouble spiral_on_cone::u_max = TWO_PI;

DCoordinate3 spiral_on_cone::d0(GLdouble u)
{
    return DCoordinate3(u * cos(u), u * sin(u), u);
}

DCoordinate3 spiral_on_cone::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(c - u * s, s + u * c, 1.0);
}

DCoordinate3 spiral_on_cone::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-2.0 * s - u * c, 2.0 * c - u * s, 0);
}

GLdouble circleCurve::u_min = 0.0;
GLdouble circleCurve::u_max = TWO_PI;

DCoordinate3 circleCurve::d0(GLdouble u)
{
    return DCoordinate3(cos(u), sin(u));
}

DCoordinate3 circleCurve::d1(GLdouble u)
{
    return DCoordinate3(-sin(u), cos(u));
}

DCoordinate3 circleCurve::d2(GLdouble u)
{
    return DCoordinate3(-cos(u), -sin(u));
}

GLdouble lissajousCurve::u_min = 0.0;
GLdouble lissajousCurve::u_max = TWO_PI;

DCoordinate3 lissajousCurve::d0(GLdouble u)
{
    return DCoordinate3(3 * cos(4 * u), 1.5 * sin(3 * u));
}

DCoordinate3 lissajousCurve::d1(GLdouble u)
{
    return DCoordinate3(-3 * sin(4 * u), 1.5 * cos(3 * u));
}

DCoordinate3 lissajousCurve::d2(GLdouble u)
{
    return DCoordinate3(-3 * cos(4 * u), -1.5 * sin(3 * u));
}

GLdouble butterflyCurve::u_min = 0;
GLdouble butterflyCurve::u_max = 12 * PI;

DCoordinate3 butterflyCurve::d0(GLdouble u)
{
    return DCoordinate3(sin(u) * (exp(cos(u)) - 2 * cos(4 * u) - pow(sin(u / 12), 5.0)),
                        cos(u) * (exp(cos(u)) - 2 * cos(4 * u) - pow(sin(u / 12), 5.0)));
}

DCoordinate3 butterflyCurve::d1(GLdouble u)
{
    return DCoordinate3(sin(u) * (8 * sin(4 * u) - exp(cos(u)) * sin(u) - ((5 * cos(u / 12) * pow(sin(u / 12), 4.0)) / 12)) + cos(u) * (-2 * cos(4 * u) + exp(cos(u)) - pow(sin(u / 12), 5.0)),
                        cos(u) * (8 * sin(4 * u) - exp(cos(u)) * sin(u) - ((5 * cos(u / 12) * pow(sin(u / 12), 4.0)) / 12)) - sin(u) * (-2 * cos(4 * u) + exp(cos(u)) - pow(sin(u / 12), 5.0)));
}

DCoordinate3 butterflyCurve::d2(GLdouble u)
{
    return DCoordinate3(
                        (2304 * cos(u) * sin(4*u) + 4896*sin(u) * cos(4*u) + 144*exp(cos(u)) * pow(sin(u), 3.0) + (-432 * exp(cos(u)) * cos(u) - 144*exp(cos(u)) + 149*pow(sin(u/12), 5.0) - 20*pow(u/12, 2.0) * pow(sin(u/12), 3.0)) * sin(u) - 120*cos(u/12) * pow(sin(u/12), 4.0) * cos(u)) / 144,
                        (120*cos(u/12)*pow(sin(u/12), 4.0)*sin(u)+57600*pow(cos(u), 5.0)+(-144*exp(cos(u))-66816)*pow(cos(u), 3.0)-432*exp(cos(u))*pow(cos(u), 2.0)+(169*pow(sin(u/12), 5.0)-20*pow(sin(u/12),3.0)+14112)*cos(u)+288*exp(cos(u)))/144
                        );
}

GLdouble hyperParabolaCurve::u_min = 0.0;
GLdouble hyperParabolaCurve::u_max = TWO_PI;

DCoordinate3 hyperParabolaCurve::d0(GLdouble u)
{
    return DCoordinate3(
                        2 * ((1 + pow(u, 2.0)) / (1 - pow(u, 2.0))) + 2,
                        2 * (2*u / (1 - pow(u, 2.0))) + 1
                        );
}

DCoordinate3 hyperParabolaCurve::d1(GLdouble u)
{
    return DCoordinate3(
                        (8*u)/pow((pow(u, 2.0)-1), 2.0),
                        (4*(pow(u, 2.0)+1))/pow((pow(u, 2.0)-1), 2.0)
                        );
}

DCoordinate3 hyperParabolaCurve::d2(GLdouble u)
{
    return DCoordinate3(
                        -(8*(3*pow(u, 2.0)+1))/pow((pow(u, 2.0)-1), 3.0),
                        -(8*u*(pow(u, 2.0)+3))/pow((pow(u, 2.0)-1), 3.0)
                        );
}

// Surface matching below
GLdouble toruszCurve::u_min = 0.0;
GLdouble toruszCurve::u_max = TWO_PI;

DCoordinate3 toruszCurve::d0(GLdouble u)
{
    return DCoordinate3(
                        (5 + 3 * sin(u)) * cos(sin(3 * u)),
                        (5 + 3 * sin(u)) * sin(sin(3 * u)),
                        5 * cos(u)
                        );
}

DCoordinate3 toruszCurve::d1(GLdouble u)
{
    return DCoordinate3(
                        (-9*sin(u)-15)*cos(3*u)*sin(sin(3*u))+3*cos(u)*cos(sin(3*u)),
                        3*cos(u)*sin(sin(3*u))+(9*sin(u)+15)*cos(3*u)*cos(sin(3*u)),
                        -5*sin(u)
                        );
}

DCoordinate3 toruszCurve::d2(GLdouble u)
{
    return DCoordinate3(
                        ((27*sin(u)+45)*sin(3*u)-18*cos(u)*cos(3*u))*sin(sin(3*u))+((-27*sin(u)-45)*pow(cos(3*u), 2.0)-3*sin(u))*cos(sin(3*u)),
                        -3*(((9*sin(u)+15)*pow(cos(3*u), 2.0)+sin(u))*sin(sin(3*u))+((9*sin(u)+15)*sin(3*u)-6*cos(u)*cos(3*u))*cos(sin(3*u))),
                        -5*cos(u)
                        );
}

GLdouble helixCurve::u_min = 2;
GLdouble helixCurve::u_max = TWO_PI;

DCoordinate3 helixCurve::d0(GLdouble u)
{
    return DCoordinate3(
                        2 * cos(u),
                        2 * sin(u),
                        3 * u
                        );
}

DCoordinate3 helixCurve::d1(GLdouble u)
{
    return DCoordinate3(
                        -2 * sin(u),
                        2 * cos(u),
                        3
                        );
}

DCoordinate3 helixCurve::d2(GLdouble u)
{
    return DCoordinate3(
                        -2 * cos(u),
                        -2 * sin(u),
                        0
                        );
}

//
// PARAMETRIC SURFACES
//

GLdouble firstSurface::u_min = 0;
GLdouble firstSurface::u_max = TWO_PI;
GLdouble firstSurface::v_min = 0;
GLdouble firstSurface::v_max = TWO_PI;

DCoordinate3 firstSurface::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((2*cos(u) + 3)*cos(v), (2*cos(u) + 3)*sin(v), 2*sin(u));
}

DCoordinate3 firstSurface::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-2*cos(v)*sin(u), -2*sin(v)*sin(u), 2*cos(u));
}

DCoordinate3 firstSurface::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(-(2*cos(u) + 3)*sin(v), (2*cos(u) + 3)*cos(v), 0);
}

GLdouble secondSurface::u_min = 0;
GLdouble secondSurface::u_max = TWO_PI;
GLdouble secondSurface::v_min = 0;
GLdouble secondSurface::v_max = TWO_PI;

DCoordinate3 secondSurface::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(3*cos(u)+(1.0/2)*(1+cos(2*u))*sin(v)-(1.0/2)*sin(2*u)*sin(2*v),
                        3*sin(u)+(1.0/2)*sin(2*u)*sin(v)-(1.0/2)*(1-cos(2*u))*sin(2*v),
                        cos(u)*sin(2*v)+sin(u)*sin(v));
}

DCoordinate3 secondSurface::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3((-sin(v)*sin(2*u))-sin(2*v)*cos(2*u)-3*sin(u),
                        (-sin(2*v)*sin(2*u))+sin(v)*cos(2*u)+3*cos(u),
                        sin(v)*cos(u)-sin(2*v)*sin(u));
}

DCoordinate3 secondSurface::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(((cos(2*u)+1)*cos(v))/2-sin(2*u)*cos(2*v),
                        (sin(2*u)*cos(v))/2-(1-cos(2*u))*cos(2*v),
                        2*cos(u)*cos(2*v)+sin(u)*cos(v));
}

GLdouble thirdSurface::u_min = -1;
GLdouble thirdSurface::u_max = 1;
GLdouble thirdSurface::v_min = -1;
GLdouble thirdSurface::v_max = 1;

DCoordinate3 thirdSurface::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(u, v, u * v);
}

DCoordinate3 thirdSurface::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(1, 0, v);
}

DCoordinate3 thirdSurface::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(0, -2 * sin(v), 2 * cos(v));
}

GLdouble forthSurface::u_min = 0;
GLdouble forthSurface::u_max = PI;
GLdouble forthSurface::v_min = 0;
GLdouble forthSurface::v_max = PI;

DCoordinate3 forthSurface::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(sin(2 * u) * cos(v) * cos(v), sin(u) * sin(2 * v) / 2, cos(2 * u) * cos(v) * cos(v));
}

DCoordinate3 forthSurface::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(cos(v) * cos(v) * 2 * cos(2 * u), sin(2 * v) / 2 * cos(u), cos(v) * cos(v) * -2 * sin(2 * u));
}

DCoordinate3 forthSurface::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(sin(2 * u) * -2 * cos(v) * sin(v), sin(u) * cos(2 * v), cos(2 * u) * -2 * cos(v) * sin(v));
}

GLdouble fifthSurface::u_min = -PI;
GLdouble fifthSurface::u_max = PI;
GLdouble fifthSurface::v_min = -PI;
GLdouble fifthSurface::v_max = PI;

DCoordinate3 fifthSurface::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3(cos(u) * cos(v), u, cos(u) * sin(v));
}

DCoordinate3 fifthSurface::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(-cos(v) * sin(u), 1, -sin(u) * sin(v));
}

DCoordinate3 fifthSurface::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3(-cos(u) * sin(v), 0, cos(u) * cos(v));
}

GLdouble sixthSurface::u_min = 0;
GLdouble sixthSurface::u_max = 3;
GLdouble sixthSurface::v_min = 0;
GLdouble sixthSurface::v_max = TWO_PI;

DCoordinate3 sixthSurface::d00(GLdouble u, GLdouble v)
{
    return DCoordinate3((1+cosh(u-3/2.0))*sin(v),
                        (1+cosh(u-3/2.0))*cos(v),
                        sinh(u-3/2.0));
}

DCoordinate3 sixthSurface::d10(GLdouble u, GLdouble v)
{
    return DCoordinate3(sin(v)*sinh(u-3/2.0),
                        cos(v)*sinh(u-3/2.0),
                        cosh(u-3/2.0));
}

DCoordinate3 sixthSurface::d01(GLdouble u, GLdouble v)
{
    return DCoordinate3((cosh(u-3/2.0)+1)*cos(v),
                        -(cosh(u-3/2.0)+1)*sin(v),
                        0);
}


